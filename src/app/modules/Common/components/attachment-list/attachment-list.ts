import { Component, Input } from '@angular/core';

@Component({
  selector: 'attachment-list',
  templateUrl: './attachment-list.html',
  styleUrls: ['./attachment-list.scss'],
})
export class AttachmentList {

  @Input()
  categoryList = [
    { name: '近景', list: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0] },
    { name: '远景', list: [1, 2, 3, 4] },
    { name: '室内', list: [1, 2, 3, 4, 5] }
  ];
}
