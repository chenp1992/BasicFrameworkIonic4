import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SearchInput } from './search-input/search-input';
import { DataListPager } from './datalist-pager/datalist-pager';
import { GoBackIcon } from './goback-icon/goback-icon';
import { AttachmentList } from './attachment-list/attachment-list';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [
    SearchInput,
    DataListPager,
    GoBackIcon,
    AttachmentList
  ],
  exports: [
    SearchInput,
    DataListPager,
    GoBackIcon,
    AttachmentList
  ]
})
export class CommonComponentModule { }
