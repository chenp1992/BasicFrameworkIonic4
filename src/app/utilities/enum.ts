
// 内容面板状态
export enum ContentPanelState {

    // 关闭
    Closed = 0,

    // 最小化
    Minimize = 1,

    // 正常
    Normal = 2,

    // 最大化
    Maximize = 3
}

// 数据视图状态
export enum DataViewStyle {
    // 未知状态
    None = 0,

    // 纵向列表
    VerList = 1,

    // 横向列表
    HorList = 2
}

// index页面状态
export enum IndexPageState {

    // 正常状态
    Normal = 0,

    // 内容隐藏状态
    HideContent = 1,

    // 正常显示内容状态
    ShowContent = 2,

    // 全屏内容状态
    FullContent = 3,

    // 仅底部工具栏状态
    OnlyBottom = 4,

    // 量测状态
    Measurement = 5
}
