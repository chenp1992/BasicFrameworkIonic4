import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'datalist-pager',
  templateUrl: './datalist-pager.html',
  styleUrls: ['./datalist-pager.scss'],
})
export class DataListPager {

  constructor() {
  }

  @Input()
  pagerInfo: Object = {
    curIndex: 15,
    totalNum: 200
  };

  @Output()
  pagerClick: EventEmitter<any> = new EventEmitter();

  dataPagerClick() {
    this.pagerClick.emit();
  }
}
