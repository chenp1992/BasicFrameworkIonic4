import { Component } from '@angular/core';

@Component({
  selector: 'goback-icon',
  templateUrl: './goback-icon.html',
  styleUrls: ['./goback-icon.scss'],
})
export class GoBackIcon {

  constructor() {
  }

  goBack() {
    window.history.back();
  }
}
