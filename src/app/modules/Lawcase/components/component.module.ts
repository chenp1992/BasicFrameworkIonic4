import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { LawcaseList } from './lawcase-list/lawcase-list';
import { LawcaseLib } from './lawcase-lib/lawcase-lib';
import { LawcaseListItem } from './lawcase-listitem/lawcase-listitem';
import { LawcaseLibItem } from './lawcase-libitem/lawcase-libitem';
import { LawcaseFlow } from './lawcase-flow/lawcase-flow';

import { CommonComponentModule } from '../../Common/components/component.module';
import { LawcaseService } from '../services/lawcase.service';
import { LawcaseSearchFilter } from './search-filter/search-filter';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommonComponentModule
  ],
  providers: [LawcaseService],
  declarations: [
    LawcaseList,
    LawcaseLib,
    LawcaseListItem,
    LawcaseLibItem,
    LawcaseFlow,
    LawcaseSearchFilter
  ],
  exports: [
    LawcaseList,
    LawcaseLib,
    LawcaseListItem,
    LawcaseLibItem,
    LawcaseFlow,
    LawcaseSearchFilter
  ]
})
export class LawcaseComponentModule { }
