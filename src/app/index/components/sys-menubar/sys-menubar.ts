import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { MenuItem } from './../../../utilities/entities/menuItem';
import { ContentPanelState, IndexPageState } from './../../../utilities/enum';
import { GlobalDataService } from '../../services/globalData.service';
import { PageStateService } from '../../services/pageState.service';


@Component({
  selector: 'sys-menubar',
  templateUrl: './sys-menubar.html',
  styleUrls: ['./sys-menubar.scss'],
})
export class SysMenuBar implements OnInit {

  private assetsPath: String = './../../../../assets/';

  selectedItem: String = null;

  // 功能菜单项配置
  sysMenuItems: Object = {
    userInfoItem: new MenuItem('userInfo', '', this.assetsPath + 'images/index/userInfo.png', ''),
    settingItem: new MenuItem('setting', '', this.assetsPath + 'images/index/setting.png', ''),
    funcItems: [
      new MenuItem('dailyPatrol', '日常巡查', this.assetsPath + 'images/index/dailyPatrol.png', ''),
      new MenuItem('middleSatellite', '中期卫片', this.assetsPath + 'images/index/middleSatellite.png', ''),
      new MenuItem('newPremises', '新增踏勘', this.assetsPath + 'images/index/newPremises.png', ''),
      new MenuItem('newProject', '新开工', this.assetsPath + 'images/index/newProject.png', ''),
      new MenuItem('changeSurvey', '变更调查', this.assetsPath + 'images/index/changeSurvey.png', ''),
      new MenuItem('yearSatellite', '年度卫片', this.assetsPath + 'images/index/yearSatellite.png', ''),
      new MenuItem('usePatrol', '利用巡查', this.assetsPath + 'images/index/usePatrol.png', ''),
      new MenuItem('lawCase', '案件查处', this.assetsPath + 'images/index/lawCase.png', '/index/lawcase')
    ]
  };

  constructor(private globalDataServie: GlobalDataService,
    private pageStateService: PageStateService,
    private router: Router) {

  }

  ngOnInit() {
    if (this.globalDataServie.curMenuItem) {
      this.menuItemClick(this.globalDataServie.curMenuItem);
    }
  }

  menuItemClick(menuItem: MenuItem) {
    if (menuItem) {
      this.selectedItem = (this.selectedItem !== menuItem.name) ? menuItem.name : null;
      if (this.selectedItem) {
        this.globalDataServie.setCurMenuItem(menuItem);
        if (menuItem.route) {
          const pageService = this.pageStateService;
          this.router.navigate([menuItem.route]).then(function () {
            pageService.setNextState(IndexPageState.ShowContent);
          });
        }
      } else {
        this.globalDataServie.setCurMenuItem(null);
        this.pageStateService.setNextState(IndexPageState.Normal);
      }
    }
  }

}
