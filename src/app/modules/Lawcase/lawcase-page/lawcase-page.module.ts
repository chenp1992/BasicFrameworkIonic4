import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RouterModule, Route } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LawcasePage } from './lawcase-page';

import { CommonComponentModule } from '../../Common/components/component.module';
import { LawcaseComponentModule } from '../components/component.module';
import { LawcaseService } from '../services/lawcase.service';

const route: Route[] = [
  {
    path: '',
    component: LawcasePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommonComponentModule,
    LawcaseComponentModule,
    RouterModule.forChild(route)
  ],
  providers: [LawcaseService],
  declarations: [
    LawcasePage
  ]
})
export class LawcasePageModule { }
