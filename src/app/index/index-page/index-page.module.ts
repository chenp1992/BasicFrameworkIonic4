import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouterModule, Routes } from '@angular/router';

import { IndexComponentModule } from '../components/component.module';

import { IndexPage } from './index-page';
import { MainMapModule } from '../../modules/Map/main-map/main-map.module';

const routes: Routes = [
  {
    path: 'index',
    component: IndexPage,
    children: [
      {
        path: 'lawcase',
        loadChildren: '../../modules/Lawcase/lawcase-page/lawcase-page.module#LawcasePageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/index',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IndexComponentModule,
    MainMapModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    IndexPage
  ]
})
export class IndexPageModule { }
