import { Injectable } from '@angular/core';

import { LawcaseItem } from '../../../utilities/entities/lawcase/lawcaseItem';
import { Promise } from 'q';
import { Subject, Observable } from 'rxjs';
import { GlobalDataService } from '../../../index/services/globalData.service';
import { LawcaseQuery } from '../../../utilities/entities/lawcase/lawcaseQuery';

@Injectable()
export class LawcaseService {

    private static _selectedItem: any = null;

    constructor(private globalDataService: GlobalDataService) {

    }

    get selectedItem(): LawcaseItem {
        return LawcaseService._selectedItem;
    }

    setSelectedItem(item: LawcaseItem) {
        LawcaseService._selectedItem = item;
    }

    get lawcaseData() {
        return [
            new LawcaseItem('20170608005', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608006', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608007', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608008', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608009', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608010', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608011', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608012', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608013', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608014', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608015', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608016', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608017', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608018', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608019', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608020', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608021', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608022', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608023', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608024', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608025', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608026', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608027', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608028', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608029', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608030', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608031', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608032', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608033', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608034', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608005', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608006', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608007', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608008', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608009', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608010', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608011', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608012', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608013', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608014', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608015', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608016', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608017', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608018', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608019', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608020', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608021', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608022', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608023', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608024', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608025', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608026', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608027', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608028', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608029', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608030', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608031', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608032', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608033', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608034', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608005', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608006', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608007', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608008', '1', '线索', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608009', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608010', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608011', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608012', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608013', '1', '立案', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608014', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608015', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608016', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608017', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608018', '1', '调查', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608019', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608020', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608021', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608022', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608023', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608024', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608025', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608026', '1', '审理', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608027', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608028', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608029', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608030', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608031', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608032', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608033', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06'),
            new LawcaseItem('20170608034', '1', '处罚', '洪山街李桥村', '武汉市源晶商品混凝土公司', '2018-06-06')
        ];
    }

    getLawcaseList(paging) {
        const currentList = [];
        return Promise((resolve, reject) => {
            if (paging) {
                const datasource = this.lawcaseData;
                setTimeout(() => {
                    if (datasource && datasource.length > 0) {
                        // 加载第一页数据
                        if (paging.pageIndex === 1) {
                            paging.itemCount = datasource.length;
                            paging.pageCount = Math.floor(datasource.length / paging.pageSize);
                            if (datasource.length % paging.pageSize > 0) {
                                paging.pageCount++;
                            }
                        } else {
                            if (paging.pageIndex > paging.pageCount) {
                                resolve(currentList);
                            }
                        }

                        // 根据页数获取数据
                        let startIndex = paging.pageSize * (paging.pageIndex - 1);
                        let endIndex = paging.pageSize * paging.pageIndex;
                        if (endIndex > datasource.length) {
                            endIndex = datasource.length;
                        }
                        while (startIndex < endIndex) {
                            currentList.push(datasource[startIndex]);
                            startIndex++;
                        }
                    } else {
                        paging.itemCount = 0;
                        paging.pageCount = 0;
                    }

                    resolve(currentList);
                }, 1500);
            } else {
                resolve(currentList);
            }
        });
    }

    setQueryParam(param: LawcaseQuery) {
        this.globalDataService.lawcaseQuery.next(param);
    }

    getQueryParam(): Observable<LawcaseQuery> {
        return this.globalDataService.lawcaseQuery.asObservable();
    }
}
