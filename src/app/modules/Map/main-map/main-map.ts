import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { loadModules, loadCss } from 'esri-loader';
// import esri = __esri;

import { MAPCONFIG } from '../../../app.config';
import { GlobalMapService } from '../services/globalMap.service';
import { GlobalDataService } from '../../../index/services/globalData.service';
import { IndexPageState } from '../../../utilities/enum';
import { PageStateService } from '../../../index/services/pageState.service';
import { Subscription } from 'rxjs';

// loadCss(MAPCONFIG.ARCGISAPI + 'esri/css/main.css');

@Component({
    selector: 'main-map',
    templateUrl: './main-map.html',
    styleUrls: ['./main-map.scss'],
})
export class MainMap implements OnInit, OnDestroy {

    private subscription: Subscription;

    showHideEle = {
        measureTool: false,
        lawcaseTool: false
    };

    @ViewChild('mainmap')
    private mapElement: ElementRef;

    @Output()
    mapLoaded = new EventEmitter<Boolean>();

    constructor(private globalMapService: GlobalMapService,
        private globalDataService: GlobalDataService,
        private pageStateService: PageStateService) {
        this.subscription = this.pageStateService.receiveState()
            .subscribe(state => {
                switch (state) {
                    case IndexPageState.Measurement:
                        this.showHideEle.measureTool = true;
                        this.showHideEle.lawcaseTool = false;
                        break;
                    case IndexPageState.OnlyBottom:
                        this.showHideEle.measureTool = false;

                        const selectedMenu = this.globalDataService.curMenuItem;
                        const curMenuName = selectedMenu ? selectedMenu.name : '';
                        if (curMenuName === 'lawCase') {
                            this.showHideEle.lawcaseTool = true;
                        }
                        break;
                    default:
                        this.showHideEle.measureTool = false;
                        this.showHideEle.lawcaseTool = false;
                        break;
                }
            });
    }

    initMap() {
        // loadModules([
        //     'esri/Map',
        //     'esri/views/MapView',
        //     'esri/Basemap',
        //     'esri/layers/TileLayer',
        //     'esri/core/urlUtils'
        // ], { url: MAPCONFIG.ARCGISAPI })
        //     .then(([Map, MapView, Basemap, TileLayer, urlUtils]) => {

        //         // 增加地图服务代理规则
        //         urlUtils.addProxyRule({
        //             urlPrefix: MAPCONFIG.SERVERADDR,
        //             proxyUrl: MAPCONFIG.ARCGISPROXY
        //         });

        //         const whBaseLayer = new TileLayer({
        //             url: 'http://192.168.5.88:6080/arcgis/rest/services/WH/WHGLQ_38/MapServer'
        //         });

        //         const whBasemap = new Basemap({
        //             baseLayers: [whBaseLayer],
        //             title: 'WuHanBasemap',
        //             id: 'whdt'
        //         });

        //         const map = new Map({
        //             basemap: whBasemap
        //         });

        //         const mapView = new MapView({
        //             container: this.mapElement.nativeElement,
        //             map: map,
        //             ui: {
        //                 components: []
        //             }
        //         });

        //         // All resources in the MapView and the map have loaded.
        //         // Now execute additional processes
        //         mapView.when(() => {
        //             mapView.goTo(whBaseLayer.fullExtent);
        //             this.globalMapService.initGlobalMap(mapView);
        //             this.mapLoaded.emit(true);
        //         });
        //     }).catch(err => {
        //         console.log('Init main map have an error: ' + err);
        //     });
    }

    ngOnInit() {
        this.initMap();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    openLayerTree() {

    }

    mapMeasure() {
        if (this.showHideEle.measureTool) {
            this.pageStateService.backPrevState();
        } else {
            this.pageStateService.setNextState(IndexPageState.Measurement);
        }
    }

    mapLocate() {

    }
}
