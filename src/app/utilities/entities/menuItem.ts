export class MenuItem {

    name: string;
    title: string;
    icon: string;
    route: string;

    constructor(name: string, title: string, icon: string, route: string) {
        this.name = name;
        this.title = title;
        this.icon = icon;
        this.route = route;
    }
}
