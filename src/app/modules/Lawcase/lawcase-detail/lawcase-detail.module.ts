import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CommonComponentModule } from '../../Common/components/component.module';
import { LawcaseComponentModule } from '../components/component.module';

import { LawcaseDetail } from './lawcase-detail';
import { LawcaseFormModule } from '../lawcase-form/lawcase-form.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommonComponentModule,
    LawcaseComponentModule,
    LawcaseFormModule,
    RouterModule.forChild([{ path: '', component: LawcaseDetail }])
  ],
  declarations: [
    LawcaseDetail
  ]
})
export class LawcaseDetailModule { }
