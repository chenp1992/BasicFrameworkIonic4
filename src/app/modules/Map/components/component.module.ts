import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { MeasureTool } from './measure-tool/measure-tool';
import { LawcaseTool } from './lawcase-tool/lawcase-tool';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule
    ],
    declarations: [
        MeasureTool,
        LawcaseTool
    ],
    exports: [
        MeasureTool,
        LawcaseTool
    ]
})
export class MapComponentModule { }
