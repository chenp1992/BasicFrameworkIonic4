import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/index';

import { ContentPanelState, IndexPageState } from '../../../utilities/enum';
import { PageStateService } from '../../services/pageState.service';

@Component({
  selector: 'content-panel',
  templateUrl: './content-panel.html',
  styleUrls: ['./content-panel.scss'],
})
export class ContentPanel implements OnDestroy {

  private subscription: Subscription;

  contentPanelState: ContentPanelState = ContentPanelState.Closed;

  showHideEle = {
    contentPanel: false,
    foldButton: false
  };

  contentPanelStyle = {
    'content-normal': false,
    'content-maximize': false
  };

  foldButtonStyle = {
    'content-btn-fold': this.contentPanelState === ContentPanelState.Normal,
    'content-btn-expand': this.contentPanelState === ContentPanelState.Minimize
  };

  constructor(private pageStateService: PageStateService) {
    // 订阅内容面板状态修改事件
    this.subscription = this.pageStateService.receiveState()
      .subscribe(state => {

        switch (state) {
          case IndexPageState.Normal:
            this.contentPanelState = ContentPanelState.Closed;
            break;
          case IndexPageState.ShowContent:
            this.contentPanelState = ContentPanelState.Normal;
            break;
          case IndexPageState.HideContent:
            this.contentPanelState = ContentPanelState.Minimize;
            break;
          case IndexPageState.FullContent:
            this.contentPanelState = ContentPanelState.Maximize;
            break;
        }

        this.setElementStyle();
      });
  }

  setElementStyle() {
    switch (this.contentPanelState) {
      case ContentPanelState.Normal:
        this.showHideEle.contentPanel = true;
        this.showHideEle.foldButton = true;

        this.contentPanelStyle['content-normal'] = true;
        this.contentPanelStyle['content-maximize'] = false;

        this.foldButtonStyle['content-btn-fold'] = true;
        this.foldButtonStyle['content-btn-expand'] = false;
        break;
      case ContentPanelState.Maximize:
        this.showHideEle.contentPanel = true;
        this.showHideEle.foldButton = false;

        this.contentPanelStyle['content-normal'] = false;
        this.contentPanelStyle['content-maximize'] = true;
        break;
      case ContentPanelState.Minimize:
        this.showHideEle.contentPanel = false;
        this.showHideEle.foldButton = true;

        this.foldButtonStyle['content-btn-fold'] = false;
        this.foldButtonStyle['content-btn-expand'] = true;
        break;
      case ContentPanelState.Closed:
        this.showHideEle.contentPanel = false;
        this.showHideEle.foldButton = false;
        break;
    }
  }

  // 改变内容面板状态
  changeContentPanelState() {
    switch (this.contentPanelState) {
      case ContentPanelState.Normal:
        this.pageStateService.setNextState(IndexPageState.HideContent);
        break;
      case ContentPanelState.Minimize:
        this.pageStateService.setNextState(IndexPageState.ShowContent);
        break;
    }
  }

  ngOnDestroy() {
    // 取消订阅
    this.subscription.unsubscribe();
  }

}
