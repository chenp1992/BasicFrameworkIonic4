import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SysMenuBar } from './sys-menubar/sys-menubar';
import { ContentPanel } from './content-panel/content-panel';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [
    SysMenuBar,
    ContentPanel
  ],
  exports: [
    SysMenuBar,
    ContentPanel
  ]
})
export class IndexComponentModule { }
