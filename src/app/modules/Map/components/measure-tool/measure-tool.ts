import { Component, AfterContentInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { GlobalMapService } from '../../services/globalMap.service';

import { loadModules, loadCss } from 'esri-loader';
// import esri = __esri;

import { MAPCONFIG } from '../../../../app.config';

@Component({
    selector: 'measure-tool',
    templateUrl: './measure-tool.html',
    styleUrls: ['./measure-tool.scss'],
})
export class MeasureTool implements AfterContentInit, OnDestroy {

    @Output()
    measureClosed: EventEmitter<any> = new EventEmitter();

    mapMeasure = {
        mode: '1',
        tooltip: '点击地图开始测距'
    };

    measureOptions: any = {
        header: '选择量测模式'
    };

    pointSymbol = {
        type: 'simple-marker',
        color: 'blue',
        size: 8,
        outline: {
            width: 0.5,
            color: 'darkblue'
        }
    };

    private globalMap = null;
    private measureEvent = null;

    constructor(private globalMapService: GlobalMapService) {
        this.globalMap = this.globalMapService.globalMap;
    }

    ngAfterContentInit() {
        this.startMeasure();
    }

    ngOnDestroy() {

    }

    measureModeChange() {

    }

    clearDraw() {

    }

    startMeasure() {
        // loadModules([
        //     'esri/Graphic'
        // ], { url: MAPCONFIG.ARCGISAPI })
        //     .then(([Graphic]) => {
        //         const measureInfo = this.mapMeasure;
        //         if (measureInfo.mode === '1') {
        //             this.measureEvent = this.globalMap.on('click', (event) => {
        //                 if (event) {
        //                     this.globalMap.graphics.add(new Graphic({
        //                         geometry: event.mapPoint,
        //                         symbol: this.pointSymbol
        //                     }));
        //                     measureInfo.tooltip = '继续点击地图测距';
        //                 }
        //             });
        //         }
        //     });
    }

    endMeasure($event) {
        if ($event) {
            $event.stopPropagation();
        }
        if (this.measureEvent) {
            this.measureEvent.remove();
        }
        this.measureClosed.emit();
    }
}
