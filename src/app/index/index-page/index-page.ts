import { Component, OnInit, OnDestroy } from '@angular/core';

import { IndexPageState } from './../../utilities/enum';
import { PageStateService } from '../services/pageState.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'index-page',
  templateUrl: './index-page.html',
  styleUrls: ['./index-page.scss'],
})
export class IndexPage implements OnInit, OnDestroy {

  indexState: IndexPageState = IndexPageState.Normal;

  showHideEle = {
    menubar: {
      visibility: 'visible'
    },
    content: {
      visibility: 'hidden'
    }
  };

  private subscription: Subscription;

  constructor(private pageStateService: PageStateService) {
    this.subscription = this.pageStateService.receiveState()
      .subscribe(state => {
        this.setShowHideEle(state);
      });
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setShowHideEle(state: IndexPageState) {
    switch (state) {
      case IndexPageState.Normal:
        this.showHideEle.menubar.visibility = 'visible';
        this.showHideEle.content.visibility = 'hidden';
        break;
      case IndexPageState.HideContent:
      case IndexPageState.ShowContent:
      case IndexPageState.FullContent:
        this.showHideEle.menubar.visibility = 'visible';
        this.showHideEle.content.visibility = 'visible';
        break;
      default:
        this.showHideEle.menubar.visibility = 'hidden';
        this.showHideEle.content.visibility = 'hidden';
        break;
    }
  }

}
