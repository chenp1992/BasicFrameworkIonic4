import { Injectable } from '@angular/core';
import { IndexPageState } from '../../utilities/enum';
import { Subject, Observable } from 'rxjs';

import * as _ from 'lodash';
import { Promise } from 'q';

@Injectable({
    providedIn: 'root'
})
export class PageStateService {

    private _indexState = {
        historyState: [],
        curState: IndexPageState.Normal
    };

    private _pageStateSub: Subject<IndexPageState> = new Subject<IndexPageState>();

    get curPageState(): IndexPageState {
        return this._indexState.curState;
    }

    get prevPageState(): IndexPageState {
        const history = this._indexState.historyState;
        if (history && history.length > 0) {
            return history[0];
        }
        return null;
    }

    setNextState(state: IndexPageState) {
        return Promise((resolve, reject) => {
            let length = this._indexState.historyState.unshift(_.clone(this._indexState.curState));
            while (length > 5) {
                this._indexState.historyState.pop();
                length = this._indexState.historyState.length;
            }
            this._indexState.curState = state;
            this._pageStateSub.next(state);
            resolve('success');
        });
    }

    backPrevState() {
        return Promise((resolve, reject) => {
            const history = this._indexState.historyState;
            if (history && history.length > 0) {
                this._indexState.curState = history.shift();
                this._pageStateSub.next(this._indexState.curState);
            }
            resolve('success');
        });
    }

    receiveState(): Observable<IndexPageState> {
        return this._pageStateSub.asObservable();
    }
}
