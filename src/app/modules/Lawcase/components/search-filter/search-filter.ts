import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'lawcase-search-filter',
  templateUrl: './search-filter.html',
  styleUrls: ['./search-filter.scss'],
})
export class LawcaseSearchFilter {

  constructor() {
  }

  @Input()
  searchFilter: Object = {
    node: '0',
    region: '0',
    time: '0'
  };

  @Output()
  filterSelectChange: EventEmitter<any> = new EventEmitter();

  nodeItems: any[] = [
    { value: '0', text: '全部' },
    { value: '1', text: '线索' },
    { value: '2', text: '立案' },
    { value: '3', text: '调查' },
    { value: '4', text: '审理' },
    { value: '5', text: '处罚' }
  ];

  nodeSelectOptions: any = {
    header: '案件节点'
  };

  areaItems: any[] = [
    { value: '0', text: '全部' },
    { value: '1', text: '江岸区' },
    { value: '2', text: '江汉区' },
    { value: '3', text: '江夏区' },
    { value: '4', text: '武昌区' },
    { value: '5', text: '汉阳区' },
    { value: '6', text: '东西湖区' }
  ];

  areaSelectOptions: any = {
    header: '案件区域'
  };

  timeItems: any[] = [
    { value: '0', text: '全部' },
    { value: '1', text: '两个月内' },
    { value: '2', text: '一个月内' },
    { value: '3', text: '半个月内' },
    { value: '4', text: '一周之内' }
  ];

  timeSelectOptions: any = {
    header: '发生时间'
  };

  selectItemChange() {
    this.filterSelectChange.emit();
  }

}
