import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RegisterClueCase } from './register-clueCase/register-clueCase';
import { FilingCaseRecord } from './filingCase-record/filingCase-record';
import { AskCaseRecord } from './askCase-record/askCase-record';
import { SurveyCaseReport } from './surveyCase-report/surveyCase-report';
import { HearCaseRecord } from './hearCase-record/hearCase-record';
import { HandleCaseDecision } from './handleCase-decision/handleCase-decision';
import { PunishCaseDecision } from './punishCase-decision/punishCase-decision';
import { ExecuteCaseRecord } from './executeCase-record/executeCase-record';
import { CloseCaseRecord } from './closeCase-record/closeCase-record';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [
    RegisterClueCase,
    FilingCaseRecord,
    AskCaseRecord,
    SurveyCaseReport,
    HearCaseRecord,
    HandleCaseDecision,
    PunishCaseDecision,
    ExecuteCaseRecord,
    CloseCaseRecord
  ],
  exports: [
    RegisterClueCase,
    FilingCaseRecord,
    AskCaseRecord,
    SurveyCaseReport,
    HearCaseRecord,
    HandleCaseDecision,
    PunishCaseDecision,
    ExecuteCaseRecord,
    CloseCaseRecord
  ]
})
export class LawcaseFormModule { }
