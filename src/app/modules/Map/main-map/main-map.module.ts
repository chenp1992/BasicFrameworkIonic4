import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MainMap } from './main-map';
import { MapComponentModule } from '../components/component.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MapComponentModule
    ],
    declarations: [
        MainMap
    ],
    exports: [
        MainMap
    ]
})
export class MainMapModule { }
