export class LawcaseItem {

    num: string;
    state: string;
    stateText: string;
    address: string;
    company: string;
    createDate: string;

    constructor(num: string, state: string, stateText: string,
        address: string = null, company: string = null, createDate: string = null) {
        this.num = num;
        this.state = state;
        this.stateText = stateText;
        this.address = address;
        this.company = company;
        this.createDate = createDate;
    }
}
