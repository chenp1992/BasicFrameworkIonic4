import { Component, AfterContentInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalDataService } from '../../../../index/services/globalData.service';

@Component({
  selector: 'lawcase-flow',
  templateUrl: './lawcase-flow.html',
  styleUrls: ['./lawcase-flow.scss'],
})
export class LawcaseFlow implements AfterContentInit {

  private selectedNode = null;
  private prevNodeIds = [];

  flowNodes = [];


  @Input()
  currentNodeId = 'clue';

  @Output()
  flowNodeClick = new EventEmitter<any>();

  constructor(private globalDataService: GlobalDataService) {
  }

  initFlowMap() {
    if (this.currentNodeId) {
      const lawcaseNodes = this.globalDataService.lawcaseNodes;
      this.getPrevNodes(this.currentNodeId);
      const nodes = lawcaseNodes.map(item => {
        return {
          nodeId: item.nodeId,
          nodeName: item.nodeName,
          prevNode: item.prevNode,
          nextNode: item.nextNode,
          enabled: this.prevNodeIds.includes(item.nodeId)
        };
      });

      let defaultNode = null;
      nodes.forEach((value, index, array) => {
        if (value.enabled) {
          value['nodeStyle'] = 'node-enabled';
          if (value.nextNode) {
            const nextNode = array.filter(function (item) {
              return item.nodeId === value.nextNode;
            });
            value['lineStyle'] = nextNode[0].enabled ? 'line-enabled' : 'line-disabled';
          }
        } else {
          value['nodeStyle'] = 'node-disabled';
          value['lineStyle'] = 'line-disabled';
        }

        if (value.nodeId === this.currentNodeId) {
          defaultNode = value;
        }
      });

      this.flowNodes = nodes;
      if (defaultNode) {
        setTimeout(() => {
          this.nodeItemClick(defaultNode, null);
        }, 0);
      }
    }
  }

  ngAfterContentInit() {
    this.initFlowMap();
  }

  getPrevNodes(nodeId) {
    const lawcaseNodes = this.globalDataService.lawcaseNodes;
    if (lawcaseNodes && lawcaseNodes.length > 0) {
      lawcaseNodes.forEach((item) => {
        if (nodeId === item.nodeId) {
          this.prevNodeIds.push(item.nodeId);
          if (item.prevNode) {
            this.getPrevNodes(item.prevNode);
          }
          return false;
        }
      });
    }
  }

  nodeItemClick(node, $event) {
    if ($event) {
      $event.stopPropagation();
    }
    const isRepeat = (this.selectedNode && this.selectedNode.nodeId === node.nodeId);
    if (isRepeat) {
      return;
    }
    this.setNodeStyle(node);
    this.selectedNode = node;
    this.flowNodeClick.emit(node);
  }

  setNodeStyle(node) {
    this.flowNodes.forEach((item) => {
      if (this.selectedNode && this.selectedNode.nodeId === item.nodeId) {
        item['nodeStyle'] = 'node-enabled';
      }
      if (node.nodeId === item.nodeId) {
        item['nodeStyle'] = 'node-selected';
      }
    });
  }

}
