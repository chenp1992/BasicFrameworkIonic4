import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LawcaseItem } from '../../../../utilities/entities/lawcase/lawcaseItem';

@Component({
  selector: 'lawcase-listitem',
  templateUrl: './lawcase-listitem.html',
  styleUrls: ['./lawcase-listitem.scss'],
})
export class LawcaseListItem {

  constructor() {
  }

  @Input()
  itemData: LawcaseItem = null;

  @Output()
  itemClick: EventEmitter<any> = new EventEmitter();

  @Output()
  openDetail: EventEmitter<any> = new EventEmitter();

  listItemClick() {
    this.itemClick.emit();
  }

  listItemDetail($event) {
    if ($event) {
      $event.stopPropagation();
    }
    this.openDetail.emit();
  }

}
