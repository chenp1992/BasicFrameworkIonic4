import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

import { DataViewStyle, IndexPageState } from '../../../utilities/enum';
import { PageStateService } from '../../../index/services/pageState.service';
import { Subscription } from 'rxjs';
import { LawcaseService } from '../services/lawcase.service';
import { LawcaseQuery } from '../../../utilities/entities/lawcase/lawcaseQuery';

@Component({
  selector: 'lawcase-page',
  templateUrl: './lawcase-page.html',
  styleUrls: ['./lawcase-page.scss'],
})
export class LawcasePage implements OnInit, AfterViewInit, OnDestroy {

  private pageStateSub: Subscription;

  private viewStyle: DataViewStyle;

  switchBtnStyle = {
    'list-icon': false,
    'lib-icon': false
  };

  dataViewStyle = {
    'verStyle': false,
    'horStyle': false
  };

  lawcaseQuery: LawcaseQuery = new LawcaseQuery();

  constructor(private pageStateService: PageStateService,
    private lawcaseService: LawcaseService) {
    this.pageStateSub = this.pageStateService.receiveState()
      .subscribe(state => {
        switch (state) {
          case IndexPageState.ShowContent:
            this.viewStyle = DataViewStyle.VerList;
            break;
          case IndexPageState.FullContent:
            this.viewStyle = DataViewStyle.HorList;
            break;
        }

        this.setElementStyle();
      });
  }

  ngOnInit() {
    this.viewStyle = DataViewStyle.VerList;
    this.setElementStyle();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.lawcaseQueryChange();
    }, 0);
  }

  setElementStyle() {
    switch (this.viewStyle) {
      case DataViewStyle.VerList:
        this.switchBtnStyle['list-icon'] = true;
        this.switchBtnStyle['lib-icon'] = false;

        this.dataViewStyle['verStyle'] = true;
        this.dataViewStyle['horStyle'] = false;
        break;
      case DataViewStyle.HorList:
        this.switchBtnStyle['list-icon'] = false;
        this.switchBtnStyle['lib-icon'] = true;

        this.dataViewStyle['verStyle'] = false;
        this.dataViewStyle['horStyle'] = true;
        break;
    }
  }

  switchDataView() {
    let pageState;
    switch (this.viewStyle) {
      case DataViewStyle.VerList:
        pageState = IndexPageState.FullContent;
        break;
      case DataViewStyle.HorList:
        pageState = IndexPageState.ShowContent;
        break;
    }
    this.pageStateService.setNextState(pageState)
      .then(() => {
        this.lawcaseQueryChange();
      });
  }

  lawcaseQueryChange() {
    this.lawcaseService.setQueryParam(this.lawcaseQuery);
    console.log(this.lawcaseQuery);
  }

  ngOnDestroy() {
    this.pageStateSub.unsubscribe();
  }
}
