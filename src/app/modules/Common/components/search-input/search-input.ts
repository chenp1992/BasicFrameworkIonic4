import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'search-input',
  templateUrl: './search-input.html',
  styleUrls: ['./search-input.scss'],
})
export class SearchInput {

  @Input()
  searchText = '';

  @Input()
  placeHolderText = '';

  @Output()
  inputTextChange: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

}
