import { Component, Input } from '@angular/core';

@Component({
  selector: 'filingCase-record',
  templateUrl: './filingCase-record.html',
  styleUrls: ['./filingCase-record.scss'],
})
export class FilingCaseRecord {

  @Input()
  formData = {
    num: '201707110016',
    source: '动态巡查',
    contact: '刘善林',
    contactNum: '13798265478',
    address: '汉阳区永丰街快活岭村',
    happenAddr: '汉阳区快活岭五组邱湾',
    content: `武汉市源锦商品混凝土公司洪山分公司，未经土地管理部门批准，于2012年10月擅自占用
              武汉市洪山区洪山街李桥村土地建设混凝土搅拌站。经现场勘测，违法占用的土地总面积为
              20823平方米（31.2345亩），其中占用农用地的耕地994平方米（1.491亩）和坑塘水面
              11424平方米（17.136亩），占用建设用地的铁路用地8405平方米（12.6亩），经核查
              《武汉市土地利用总体规划（2006-2020年）》`
  };
}
