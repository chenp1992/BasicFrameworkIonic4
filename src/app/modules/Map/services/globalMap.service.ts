import { Injectable, OnDestroy } from '@angular/core';
// import esri = __esri;

@Injectable({
    providedIn: 'root'
})
export class GlobalMapService implements OnDestroy {

    private _globalMap = null;

    initGlobalMap(mapView) {
        this._globalMap = mapView;
    }

    get globalMap() {
        return this._globalMap;
    }

    // 地图定位
    mapLocate(geometry) {

    }

    // 启动量测
    startMeasure() {
        this.globalMap.on('click', function (event) {

        });
    }

    // 结束量测
    endMeasure() {

    }

    ngOnDestroy() {

    }
}
