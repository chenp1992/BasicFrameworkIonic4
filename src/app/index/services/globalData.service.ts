import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MenuItem } from '../../utilities/entities/menuItem';
import { LawcaseNode } from '../../utilities/entities/lawcase/lawcaseNode';
import { LawcaseQuery } from '../../utilities/entities/lawcase/lawcaseQuery';

@Injectable({
    providedIn: 'root'
})
export class GlobalDataService {

    private _curMenuItem: MenuItem = null;

    lawcaseQuery: Subject<LawcaseQuery> = new Subject<LawcaseQuery>();

    get curMenuItem(): MenuItem {
        return this._curMenuItem;
    }

    setCurMenuItem(menuItem: MenuItem) {
        this._curMenuItem = menuItem;
    }

    get lawcaseNodes(): LawcaseNode[] {
        return [
            new LawcaseNode('clue', '线索', null, 'filing'),
            new LawcaseNode('filing', '立案', 'clue', 'survey'),
            new LawcaseNode('survey', '调查', 'filing', 'hear'),
            new LawcaseNode('hear', '审理', 'survey', 'handle'),
            new LawcaseNode('handle', '处理决定', 'hear', 'punish'),
            new LawcaseNode('punish', '处罚决定', 'handle', 'execute'),
            new LawcaseNode('execute', '执行', 'punish', 'close'),
            new LawcaseNode('close', '结案', 'execute', null)
        ];
    }
}
