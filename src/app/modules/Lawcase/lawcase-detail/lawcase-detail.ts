import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lawcase-detail',
  templateUrl: './lawcase-detail.html',
  styleUrls: ['./lawcase-detail.scss'],
})
export class LawcaseDetail implements OnInit {

  selectedForm = undefined;

  currentNode = {
    nodeId: 'punish'
  };

  private lawcaseForms = {
    clue: [
      { id: 'register-clueCase', name: '违法线索登记表', url: '', submit: this.currentNode.nodeId === 'clue' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    filing: [
      { id: 'filingCase-record', name: '立案呈批表', url: '', submit: this.currentNode.nodeId === 'filing' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    survey: [
      { id: 'askCase-record', name: '询问笔录', url: '', submit: this.currentNode.nodeId === 'survey' },
      { id: 'surveyCase-report', name: '调查报告', url: '', submit: this.currentNode.nodeId === 'survey' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    hear: [
      { id: 'hearCase-record', name: '违法案件审理记录', url: '', submit: this.currentNode.nodeId === 'hear' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    handle: [
      { id: 'handleCase-decision', name: '处理决定呈批表', url: '', submit: this.currentNode.nodeId === 'handle' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    punish: [
      { id: 'punishCase-decision', name: '行政处罚决定书', url: '', submit: this.currentNode.nodeId === 'punish' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    execute: [
      { id: 'executeCase-record', name: '执行记录', url: '', submit: this.currentNode.nodeId === 'execute' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ],
    close: [
      { id: 'closeCase-record', name: '结案呈批表', url: '', submit: this.currentNode.nodeId === 'close' },
      { id: 'attachment', name: '附件', url: '', submit: false  }
    ]
  };

  currentForms = [];

  constructor() {

  }

  ngOnInit() {
  }

  checkNodeChanged($event) {
    if ($event) {
      this.setCurrentForms($event.nodeId);
    }
  }

  setCurrentForms(nodeId) {
    this.currentForms = this.lawcaseForms[nodeId];
    if (this.currentForms && this.currentForms.length > 0) {
      this.selectedForm = this.currentForms[0];
    }
  }

  checkFormChanged(form, $event) {
    if ($event) {
      $event.stopPropagation();
    }
    const isRepeat = (this.selectedForm && this.selectedForm.id === form.id);
    if (isRepeat) {
      return;
    }
    this.selectedForm = form;
  }
}
