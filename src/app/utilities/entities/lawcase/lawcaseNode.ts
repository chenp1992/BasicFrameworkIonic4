export class LawcaseNode {

    nodeId: string;
    nodeName: string;
    prevNode: string;
    nextNode: string;

    constructor(nodeId: string, nodeName: string, prevNode: string = null, nextNode: string = null) {
        this.nodeId = nodeId;
        this.nodeName = nodeName;
        this.prevNode = prevNode;
        this.nextNode = nextNode;
    }
}
