import { Component, Input, Output, EventEmitter } from '@angular/core';
import { LawcaseItem } from '../../../../utilities/entities/lawcase/lawcaseItem';

@Component({
  selector: 'lawcase-libitem',
  templateUrl: './lawcase-libitem.html',
  styleUrls: ['./lawcase-libitem.scss'],
})
export class LawcaseLibItem {

  constructor() {
  }

  @Input()
  itemData: LawcaseItem = null;

  @Output()
  itemDetail: EventEmitter<any> = new EventEmitter();

  @Output()
  itemLocate: EventEmitter<any> = new EventEmitter();

  openDetail() {
    this.itemDetail.emit();
  }

  locatePos($event) {
    if ($event) {
      $event.stopPropagation();
    }
    this.itemLocate.emit();
  }

}
