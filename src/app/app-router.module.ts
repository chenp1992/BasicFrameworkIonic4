import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: './index/index-page/index-page.module#IndexPageModule'
  },
  {
    path: 'lawcase/detail/:id',
    loadChildren: './modules/Lawcase/lawcase-detail/lawcase-detail.module#LawcaseDetailModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
