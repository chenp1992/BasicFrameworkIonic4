import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LawcaseItem } from '../../../../utilities/entities/lawcase/lawcaseItem';
import { LawcaseService } from '../../../Lawcase/services/lawcase.service';
import { PageStateService } from '../../../../index/services/pageState.service';

@Component({
  selector: 'lawcase-tool',
  templateUrl: './lawcase-tool.html',
  styleUrls: ['./lawcase-tool.scss'],
  providers: [LawcaseService]
})
export class LawcaseTool implements OnInit {

  lawcaseData: LawcaseItem = new LawcaseItem('', '', '');

  itemStateStyle = {
    'state-xiansuo': false,
    'state-lian': false,
    'state-diaocha': true,
    'state-shenli': false,
    'state-chufa': false,
    'state-jiean': false
  };

  constructor(private router: Router,
    private lawcaseService: LawcaseService,
    private pageStateService: PageStateService) {
  }

  ngOnInit() {
    if (this.lawcaseService.selectedItem != null) {
      this.lawcaseData = this.lawcaseService.selectedItem;
      this.setItemStateStyle();
    }
  }

  setItemStateStyle() {
    this.itemStateStyle['state-chufa'] = true;
    this.itemStateStyle['state-diaocha'] = false;
    this.itemStateStyle['state-jiean'] = false;
    this.itemStateStyle['state-lian'] = false;
    this.itemStateStyle['state-shenli'] = false;
    this.itemStateStyle['state-xiansuo'] = false;
  }

  openDetail() {
    this.router.navigate(['/lawcase/detail', 5]);
  }

  navigate() {

  }

  goBack() {
    this.pageStateService.backPrevState();
  }
}
