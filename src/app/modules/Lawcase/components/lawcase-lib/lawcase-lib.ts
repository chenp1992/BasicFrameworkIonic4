import { Component, ViewChild, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { InfiniteScroll } from '@ionic/angular';
import { Subscription } from 'rxjs';

import { IndexPageState } from './../../../../utilities/enum';
import { LawcaseItem } from '../../../../utilities/entities/lawcase/lawcaseItem';
import { LawcaseService } from '../../services/lawcase.service';
import { PageStateService } from '../../../../index/services/pageState.service';
import { LawcaseQuery } from '../../../../utilities/entities/lawcase/lawcaseQuery';

@Component({
  selector: 'lawcase-lib',
  templateUrl: './lawcase-lib.html',
  styleUrls: ['./lawcase-lib.scss'],
})
export class LawcaseLib implements OnDestroy {

  @ViewChild(InfiniteScroll)
  infiniteScroll: InfiniteScroll;

  private querySub: Subscription;

  private queryParam = {
    pageIndex: 0,
    pageSize: 16,
    pageCount: 0,
    itemCount: 0,
    condition: null
  };

  private currentList: LawcaseItem[] = [];

  viewData = {
    isLoading: false,
    dataSource: this.currentList
  };

  constructor(private router: Router,
    private zone: NgZone,
    private pageStateService: PageStateService,
    private lawcaseService: LawcaseService) {
    this.querySub = this.lawcaseService.getQueryParam()
      .subscribe((query: LawcaseQuery) => {
        // 重置数据源列表
        this.zone.run(() => {
          this.viewData.dataSource = [];
          this.viewData.isLoading = true;
          this.infiniteScroll.disabled = true;
        });
        this.queryParam.condition = query;
        this.loadMoreData(null, true);
      });
  }

  loadMoreData($event, firstLoad = false) {
    this.queryParam.pageIndex++;
    this.lawcaseService.getLawcaseList(this.queryParam)
      .then((result: LawcaseItem[]) => {
        if (result && result.length > 0) {
          this.zone.runOutsideAngular(() => {
            this.viewData.dataSource = this.viewData.dataSource.concat(result);
          });
          this.zone.run(() => {
            // 首次加载数据需要隐藏顶部loading
            if (firstLoad) {
              this.viewData.isLoading = false;
            }
            this.viewData.dataSource = this.viewData.dataSource;
          });
          if ($event) {
            $event.target.complete();
          }
          this.infiniteScroll.disabled = (this.queryParam.pageIndex === this.queryParam.pageCount);
        }
      });
  }

  openDetail(item: LawcaseItem) {
    this.router.navigate(['/lawcase/detail', item.num]);
  }

  locateItem(item) {
    this.lawcaseService.setSelectedItem(item);
    this.pageStateService.setNextState(IndexPageState.OnlyBottom);
  }

  ngOnDestroy() {
    this.querySub.unsubscribe();
  }
}
